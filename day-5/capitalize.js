const makeTitle = (str) => {
  let words = str.split(" ");
  let capitalizedWord = words.map((word) => {
    let firstChar = word.charAt(0).toUpperCase();
    let restOfWord = word.slice(1);
    return firstChar + restOfWord;
  });
  console.log(capitalizedWord.join(" "));
};

makeTitle("This is a title");
makeTitle("capitalize every word");
makeTitle("I Like Pizza");
makeTitle("PIZZA PIZZA PIZZA");
makeTitle("I am a title");
makeTitle("I AM A TITLE");
makeTitle("i aM a tITLE");
makeTitle("the first letter of every word is capitalized");
makeTitle("I Like Pizza");
makeTitle("Don't count your ChiCKens BeFore They HatCh");
makeTitle("All generalizations are false, including this one");
makeTitle("Me and my wife lived happily for twenty years and then we met.");
makeTitle("There are no stupid questions, just stupid people.");
makeTitle("1f you c4n r34d 7h15, you r34lly n33d 2 g37 l41d");
