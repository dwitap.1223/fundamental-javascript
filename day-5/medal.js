const awardPrizes = (obj) => {
  const sorted = Object.entries(obj).sort((a, b) => b[1] - a[1]);

  const award = {};

  sorted.forEach((p, i) => {
    const [name] = p;
    if (i === 0) {
      award[name] = "Gold";
    } else if (i === 1) {
      award[name] = "Silver";
    } else if (i === 2) {
      award[name] = "Bronze";
    } else {
      award[name] = "Participation";
    }
  });
  console.log(award);
};

awardPrizes({
  Joshua: 45,
  Alex: 39,
  Eric: 43,
});

awardPrizes({
  "Person A": 1,
  "Person B": 2,
  "Person C": 3,
  "Person D": 4,
  "Person E": 102,
});

awardPrizes({
  Mario: 99,
  Luigi: 100,
  Yoshi: 299,
  Toad: 2,
});
