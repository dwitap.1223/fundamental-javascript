function calculateScore(games) {
  let aScore = 0;
  let bScore = 0;

  for (const game of games) {
    const aTurn = game[0];
    const bTurn = game[1];

    if (
      (aTurn === "R" && bTurn === "S") ||
      (aTurn === "S" && bTurn === "P") ||
      (aTurn === "P" && bTurn === "R")
    ) {
      aScore++;
    } else if (
      (bTurn === "R" && aTurn === "S") ||
      (bTurn === "S" && aTurn === "P") ||
      (bTurn === "P" && aTurn === "R")
    ) {
      bScore++;
    }
  }

  if (aScore > bScore) {
    console.log("Abigail");
  } else if (bScore > aScore) {
    console.log("Benson");
  } else {
    console.log("Tie");
  }
}

calculateScore([
  ["R", "P"],
  ["R", "S"],
  ["S", "P"],
]);
calculateScore([
  ["R", "R"],
  ["S", "S"],
]);
calculateScore([
  ["S", "R"],
  ["R", "S"],
  ["R", "R"],
]);
calculateScore([
  ["R", "P"],
  ["R", "S"],
  ["S", "P"],
]);
calculateScore([
  ["R", "R"],
  ["S", "S"],
]);
calculateScore([
  ["S", "R"],
  ["R", "S"],
  ["R", "R"],
]);
calculateScore([
  ["S", "R"],
  ["P", "R"],
]);
calculateScore([
  ["S", "S"],
  ["S", "P"],
  ["R", "S"],
  ["S", "R"],
  ["R", "R"],
]);
calculateScore([
  ["S", "R"],
  ["S", "R"],
  ["S", "R"],
  ["R", "S"],
  ["R", "S"],
]);
