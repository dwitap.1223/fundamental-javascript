const countdown = async (seconds) => {
  let timer = setInterval(() => {
    seconds--;

    if (seconds < 0) {
      clearInterval(timer);
    } else {
      const hours = Math.floor(seconds / 3600);
      const minutes = Math.floor((seconds % 3600) / 60);
      const remainingSeconds = seconds % 60;
      console.log(
        formatTime(hours),
        formatTime(minutes),
        formatTime(remainingSeconds)
      );
    }
  }, 1000);
};

function formatTime(time) {
  return time < 10 ? "0" + time : time;
}

export default countdown;
