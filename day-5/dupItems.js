const removeDups = (arr) => {
  const diffItems = [];
  const setItems = new Set();

  for (const item of arr) {
    if (!setItems.has(item)) {
      diffItems.push(item);
      setItems.add(item);
    }
  }

  console.log(diffItems);
};

removeDups([1, 0, 1, 0]);
removeDups(["The", "big", "cat"]);
removeDups(["John", "Taylor", "John"]);
removeDups(["John", "Taylor", "John", "john"]);
removeDups([
  "javascript",
  "python",
  "python",
  "ruby",
  "javascript",
  "c",
  "ruby",
]);
removeDups([1, 2, 2, 2, 3, 2, 5, 2, 6, 6, 3, 7, 1, 2, 5]);
removeDups(["#", "#", "%", "&", "#", "$", "&"]);
removeDups([3, "Apple", 3, "Orange", "Apple"]);
