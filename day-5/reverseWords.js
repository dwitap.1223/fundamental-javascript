const reverseWords = (str) => {
  const reversedStr = str.trim().split("/s+/").reverse().join(" ");

  console.log(reversedStr);
};

reverseWords(" the sky is blue");
reverseWords("hello   world!  ");
reverseWords("a good example");
reverseWords("hello world!");
reverseWords("blue is sky the");
reverseWords("a good example");
reverseWords("fraud! of example another is this");
reverseWords("man! the are You");
