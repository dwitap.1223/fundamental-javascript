// function myFunction(a, b) {
//   var regex = (a, "g");
//   var matches = b.match(regex);
//   console.log(matches ? matches.length : 0);
// }

// function myFunction(a, b) {
//   console.log(b.split(a).length - 1);
// }

// myFunction("m", "how many times does the character occur in this sentence?");
// myFunction("h", "how many times does the character occur in this sentence?");
// myFunction("?", "how many times does the character occur in this sentence?");
// myFunction("z", "how many times does the character occur in this sentence?");

// =========================
// function myFunction(a) {
//   var digits = String(a).split("").map(Number);

//   console.log(digits);
// }
// myFunction(931);

// =========================
// function myFunction(a, b) {
//   console.log(a.filter((cur) => cur !== b));
// }

// myFunction([1, 2, 3], 2);

// =========================
// function myFunction(a, b) {
//   console.log(a.filter((el) => el < 0).length);
// }

// myFunction([1, -2, 2, -4]);

// =========================
// function myFunction(a, b) {
//   console.log(a.reduce((acc, cur) => acc + cur, 0));
// }

// myFunction([10, 100, 40]);

// =========================
// function myFunction(a, b) {
//   console.log(a.reduce((acc, cur) => acc + cur, 0) / a.length);
// }

// myFunction([10, 100, 40]);

// =========================
// function myFunction(arr) {
//     let firstElement = arr[0];

//     for (var i = 1; i < arr.length; i++) {
//       if (arr[i] !== firstElement) {
//         return false;
//       }
//     }

//     return true;
//   }

// myFunction(["test", "test", "test"]);
// myFunction([1, 1, 1, 2]);
// myFunction(["10", 10, 10, 10]);

// =========================
// function myFunction(...arrays) {
//   console.log(arrays.flat());
// }

// myFunction(["a", "b", "c"], [4, 5, 6]);
// myFunction([true, true], [1, 2], ["a", "b"]);

// =========================
// function myFunction(a, b) {
//   var mergedArray = a.concat(b);
//   var uniqueArray = Array.from(new Set(mergedArray));
//   var sortedArray = uniqueArray.sort(function (a, b) {
//     return a - b;
//   });

//   console.log(sortedArray);
// }

// =========================
// function myFunction(a, b) {
//   let satu = a.concat(b);

//   let dua = satu.sort((a, b) => a - b);

//   let tiga = [];
//   dua.forEach((e, i) => {
//     if (e !== dua[i - 1]) {
//       tiga.push(e);
//     }
//   });
//   console.log(tiga);
// }

// myFunction([-10, 22, 333, 42], [-11, 5, 22, 41, 42]);

// =========================
// function myFunction(a, b) {
//   if (a.hasOwnProperty(b)) {
//     console.log(true);
//   }
//   console.log(false);
// }
// myFunction({ a: 1, b: 2, c: 3 }, "b");
// myFunction({ x: "a", y: "b", z: "c" }, "a");

// =========================
// function myFunction(a, b) {
//   console.log(Boolean(a[b]));
// }

// myFunction({ a: 1, b: 2, c: 3 }, "b");
// myFunction({ x: "a", y: null, z: "c" }, "y");

// =========================
// function myFunction(a) {
// console.log({ key: a });
// }

// myFunction("a");

// =========================
// function myFunction(a, b) {
//   console.log({ [a]: b });
// }

// myFunction("a", "b");

// =========================
// function myFunction(arr) {
//   const sort = (x, y) => x.b - y.b;
//   console.log(arr.sort(sort));
// }

// myFunction([
//   { a: 1, b: 7 },
//   { a: 2, b: 1 },
// ]);

// =========================
// function myFunction(x, y) {
//   const { b, ...rest } = y;
//   console.log({ ...x, ...rest, d: b });
// }

// myFunction({ a: 1, b: 2 }, { c: 3, b: 4, e: 5 });
// myFunction({ a: 5, b: 4 }, { c: 3, b: 1, e: 2 });
