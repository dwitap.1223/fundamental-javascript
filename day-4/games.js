const leftSlide = (arr) => {
  const result = [];

  arr.forEach((e) => {
    if (e !== 0) {
      result.push(e);
    }
  });

  while (result.length < arr.length) {
    result.push(0);
  }

  for (let i = 0; i < result.length - 1; i++) {
    if (result[i] === result[i + 1]) {
      result[i] *= 2;
      result.splice(i + 1, 1);
      result.push(0);
    }
  }

  console.log(result);
};

leftSlide([2, 2, 2, 0]);
leftSlide([2, 2, 4, 4, 8, 8]);
leftSlide([0, 2, 0, 2, 4]);
leftSlide([0, 2, 2, 8, 8, 8]);
leftSlide([2, 2, 2, 0]);
leftSlide([2, 2, 4, 4, 8, 8]);
leftSlide([0, 2, 0, 2, 4]);
leftSlide([0, 2, 2, 8, 8, 8]);
leftSlide([0, 0, 0, 0]);
leftSlide([0, 0, 0, 2]);
leftSlide([2, 0, 0, 0]);
leftSlide([8, 2, 2, 4]);
leftSlide([1024, 1024, 1024, 512, 512, 256, 256, 128, 128, 64, 32, 32]);
