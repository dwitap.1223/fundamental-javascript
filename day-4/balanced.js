const balanced = async (str) => {
  const round = Math.floor(str.length / 2);
  const str1 = str.slice(0, round).split("");
  const str2 = str.slice(-round).split("");

  let result1 = 0;
  let result2 = 0;

  str1.forEach((e) => {
    const alphaVal = e.toLowerCase().charCodeAt(0) - 97 + 1;
    result1 += alphaVal;
  });

  str2.forEach((e) => {
    const alphaVal = e.toLowerCase().charCodeAt(0) - 97 + 1;
    result2 += alphaVal;
  });

  result1 === result2 ? console.log(true) : console.log(false);
};

balanced("zips");
balanced("brake");
// balanced("at");
// balanced("forgetful");
// balanced("vegetation");
// balanced("disillusioned");
// balanced("abstract");
// balanced("clever");
// balanced("conditionalities");
// balanced("seasoning");
// balanced("uptight");
// balanced("ticket");
// balanced("calculate");
// balanced("measure");
// balanced("join");
// balanced("anesthesiologies");
// balanced("command");
// balanced("graphite");
// balanced("quadratically");
// balanced("right");
// balanced("fossil");
// balanced("sparkling");
// balanced("dolphin");
// balanced("baseball");
// balanced("immense");
// balanced("pattern");
// balanced("hand");
// balanced("radar");
// balanced("oven");
// balanced("immutability");
// balanced("kayak");
// balanced("bartender");
// balanced("weight");
// balanced("lightbulbs");
// balanced("tail");
