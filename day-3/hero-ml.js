import fetch from "node-fetch";

const url = "https://api.dazelpro.com/mobile-legends/hero";

const getHero = async () => {
  const response = await fetch(url);
  const data = await response.json();

  console.log(data);
};

export default getHero;
