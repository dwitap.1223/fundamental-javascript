import fetch from "node-fetch";

const fetchAnimeRecommendations = async () => {
  const url = "https://api.jikan.moe/v4/anime";

  try {
    const response = await fetch(url);
    const data = await response.json();
    const splitData = data.data.slice(0, 10);
    d;

    // 1. Show all list title
    const allTitles = splitData.map((item) => item.title);

    console.log("All Titles:", allTitles);

    // 2. Sort title by date release
    const sortedDate = splitData.sort(
      (a, b) => new Date(a.aired.from) - new Date(b.aired.from)
    );
    const sortedTitles = sortedDate.map((item) => item.title);

    console.log("Titles sorted by release date:", sortedTitles);

    // 3. Show 5 most popular anime from recommendation
    const sortedPopularity = splitData.sort(
      (a, b) => b.popularity - a.popularity
    );
    const titlesByPopularity = sortedPopularity
      .map(
        (item) => " Title : " + item.title + ", Popularity :" + item.popularity
      )
      .slice(0, 5);

    console.log("5 Most Popular Anime:", titlesByPopularity);

    // 4. Show 5 high rank anime from recommendation
    const sortedRank = splitData.sort((a, b) => a.rank - b.rank);
    const titlesByRank = sortedRank
      .map((item) => " Title : " + item.title + ", Rank :" + item.rank)
      .slice(0, 5);

    console.log("5 High-Ranked Anime:", titlesByRank);

    // 5. Show most episodes anime from recommendation
    const sortedEpisodes = splitData.sort((a, b) => b.episodes - a.pepisodes);
    const titleByEpisodes = sortedEpisodes
      .map((item) => " Title : " + item.title + ", Episodes :" + item.episodes)
      .slice(0, 1);

    console.log("Anime with the Most Episodes:", titleByEpisodes);
  } catch (error) {
    console.error("Error:", error);
  }
};

export default fetchAnimeRecommendations;
