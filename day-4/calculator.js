const calculator = async (a, b, z) => {
  let operation = z.toLowerCase();
  a = Number(a);
  b = Number(b);
  switch (operation) {
    case "add":
      console.log(Math.floor(a + b));
      break;
    case "subtract":
      console.log(Math.floor(a - b));
      break;
    case "multiply":
      console.log(Math.floor(a * b));
      break;
    case "divide":
      if (a / b == "Infinity") {
        console.log(undefined);
      } else {
        console.log(Math.floor(a / b));
      }
      break;
    default:
      console.log("input is not match");
  }
};

calculator("0.98", "2", "add");
calculator("4", "5", "subtract");
calculator("6", "3", "divide");
calculator("2", "7", "multiply");
calculator("6", "0", "divide");
calculator("60", "0", "plus");
