const emotify = (sentence) => {
  const emoticons = {
    smile: ":D",
    grin: ":)",
    sad: ":(",
    mad: ":P",
  };

  const result = sentence
    .toLowerCase()
    .replace(/(smile|grin|sad|mad)/g, (match) => emoticons[match]);

  console.log(result);
};

emotify("Make me smile");
emotify("Make me grin");
emotify("Make me sad");
emotify("Make me SmIle");
emotify("Make me grIn");
emotify("Make me SAD");
emotify("Make me MaD");
