const numInStr = (arr) => {
  console.log(
    arr.filter((str) => {
      for (let i = 0; i < str.length; i++) {
        const charCode = str.charCodeAt(i);
        if (charCode >= 48 && charCode <= 57) {
          return true;
        }
      }
      return false;
    })
  );
};

numInStr(["1a", "a", "2b", "b"]);
numInStr(["abc", "abc10"]);
numInStr(["abc", "ab10c", "a10bc", "bcd"]);
numInStr(["this is a test", "test1"]);
numInStr(["abc", "ab10c", "a10bc", "bcd"]);
numInStr(["1", "a", " ", "b"]);
numInStr(["rct", "ABC", "Test", "xYz"]);
numInStr(["this IS", "10xYZ", "xy2K77", "Z1K2W0", "xYz"]);
numInStr(["-/>", "10bc", "abc "]);
