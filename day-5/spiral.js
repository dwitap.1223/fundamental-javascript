const spiralOrder = (matrix) => {
  let right = matrix[0];
  let down = matrix[1].slice(-1);
  let left = matrix[2].reverse();
  let up = matrix[1].slice(0, -1);

  let result = [right, down, left, up].flat();

  console.log(result);
};

spiralOrder([
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
]);

spiralOrder([
  [1, 2, 3, 4],
  [5, 6, 7, 8],
  [9, 10, 11, 12],
]);

spiralOrder([
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
]);

spiralOrder([
  [1, 2, 3, 4],
  [5, 6, 7, 8],
  [9, 10, 11, 12],
]);

spiralOrder([
  [1, 2, 3, 4, 5],
  [6, 7, 8, 9, 10],
  [11, 12, 13, 14, 15],
]);

spiralOrder([
  [1, 2, 3, 4, 5, 6],
  [7, 8, 9, 10, 11, 12],
  [13, 14, 15, 16, 17, 18],
]);

spiralOrder([
  [13, 14, 15, 16, 17, 18],
  [1, 2, 3, 4, 5, 6],
  [7, 8, 9, 10, 11, 12],
]);
spiralOrder([
  [13, 14, 15, 16, 17, 18, 19, 20, 21, 22],
  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
  [7, 8, 9, 10, 11, 12, 23, 24, 25, 26],
]);
