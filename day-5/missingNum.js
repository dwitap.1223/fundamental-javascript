const missingNum = (arr) => {
  const expectedSum = 55;
  const sumArray = arr.reduce((sum, num) => sum + num, 0);
  console.log(expectedSum - sumArray);
};

missingNum([1, 2, 3, 4, 6, 7, 8, 9, 10]);
missingNum([7, 2, 3, 6, 5, 9, 1, 4, 8]);
missingNum([10, 5, 1, 2, 4, 6, 8, 3, 9]);
missingNum([1, 2, 3, 4, 6, 7, 8, 9, 10]);
missingNum([7, 2, 3, 6, 5, 9, 1, 4, 8]);
missingNum([7, 2, 3, 9, 4, 5, 6, 8, 10]);
missingNum([10, 5, 1, 2, 4, 6, 8, 3, 9]);
missingNum([1, 7, 2, 4, 8, 10, 5, 6, 9]);
