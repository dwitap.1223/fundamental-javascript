function finalCountdown(arr) {
  const sequences = [];
  let count = 0;
  let currentSequence = [];

  for (let i = 0; i < arr.length; i++) {
    if (arr[i] - 1 === arr[i + 1]) {
      currentSequence.push(arr[i]);
    } else if (arr[i] === 1) {
      currentSequence.push(arr[i]);
      sequences.push(currentSequence);
      currentSequence = [];
      count++;
    }
  }

  console.log([count, sequences]);
}

finalCountdown([4, 8, 3, 2, 1, 2]);
finalCountdown([4, 4, 5, 4, 3, 2, 1, 8, 3, 2, 1]);
finalCountdown([4, 3, 2, 1, 3, 2, 1, 1]);
finalCountdown([1, 1, 2, 1]);
finalCountdown([]);
finalCountdown([5, 4, 3, 2, 1]);
finalCountdown([2, 5, 4, 3, 2, 1, 2]);
finalCountdown([2, 3, 2, 1, 4, 3, 2, 1]);
finalCountdown([4, 3, 2, 5, 4, 3]);
finalCountdown([4, 3, 2, 5, 4, 3, 1]);
finalCountdown([3, 2, 1, 5, 5, 3, 2, 1, 5, 5]);
finalCountdown([4, 8, 3, 2, 1, 2]);
finalCountdown([4, 4, 5, 4, 3, 2, 1, 8, 3, 2, 1]);
finalCountdown([4, 3, 2, 1, 3, 2, 1, 1]);
finalCountdown([1, 2, 1, 1]);
finalCountdown([1, 2, 3, 4, 3, 2, 1]);
finalCountdown([]);
finalCountdown([2, 1, 2, 1]);
